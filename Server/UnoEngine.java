package Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import Server.Player;
import Components.*;

public class UnoEngine extends Thread {
	
	private int TRUE = 1;
	private int FALSE =0;
	private int WRONGPLUS4 =-1;
	private int WRONGUNO =-2;
	private int WINNER =2;
	

	private ArrayList<Player> playerList;
	private int numberOfPlayers, activePlayerIndex;
	private int gameNbr;
	private Deck deck;
	private boolean clockWise; // direction to play
	private Player activePlayer;
	private MailBox mb;	//communicate
	
	private int topCardValue;	// valu of the card in play
	private int topCardColor;	// color of the card in play
	private int oldValue;		// value of last card active player played
	private boolean jumpPlayer;	// if next player should be skiped
	private boolean turnOk;		// if active player have made a valid round or turn
	private int cardsDrawn;		// how many cards active player have picked up
	private int nbrOfPlayerToStart; //Number of players needed to start, used by ServerMain.
	private Player winner;
	
	
	
	/**
	 * Constructor
	 * 
	 * @param playerList list of players
	 * @param gameNbr The number of the game;
	 * @param mb	connecitons thread for communication
	 */
	public UnoEngine(ArrayList<Player> playerList, MailBox mb, int gameNbr) {
		this.playerList = playerList;
		activePlayerIndex = 0;
		activePlayer = null;
		clockWise = true;
		this.mb=mb;
		this.gameNbr=gameNbr;
		nbrOfPlayerToStart=8;
		winner=null;
		
		
	}


	/**
	 * Init function
	 * @throws IOException
	 */
	public void run(){

		numberOfPlayers = playerList.size();
		deck = new Deck();
		System.out.println("Deck is created.");
		
		//get first card (not a wildcard)
		while(deck.peekCardInPlay().getValue()>9){
			deck.setCardInPlay(deck.deal());
		}
		deck.shuffle();
		

		//save the values
		topCardValue = deck.peekCardInPlay().getValue();
		topCardColor = deck.peekCardInPlay().getColor();
		
		//Check so that all have set there name
		int numberOfPlayersWithName=0;
		while(numberOfPlayersWithName<numberOfPlayers){
			numberOfPlayersWithName=0;
			for (Player p : playerList) {
				System.out.println("numberOfReadyPlayer: "+ numberOfPlayersWithName);
				try {
					if(p.hasName()){
						numberOfPlayersWithName++;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println("number of players:" + numberOfPlayers);
		try {
			dealStartHands();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Pick random first player
		Random r = new Random();
		activePlayerIndex = r.nextInt(numberOfPlayers);
		setNextPlayer();
		

		//Loop to check that the players are ready to start to play.
		int numberOfReadyPlayers=0;
		while(numberOfReadyPlayers<numberOfPlayers){
			numberOfReadyPlayers=0;
			for (Player p : playerList) {
				System.out.println("numberOfReadyPlayer: "+ numberOfReadyPlayers);
				try {
					if(p.isReady()){
						numberOfReadyPlayers++;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		//Main game loop. isActeive checks if game has no winner
		while(!hasWinner()){
			
			oldValue=-1; 	//no last card played
			turnOk=false;	//not a valid round
			cardsDrawn=0;	//no cards drawn
			if(jumpPlayer){	 //skip one player if a skip och plus card have been played
				setNextPlayer();
				jumpPlayer=false;
			}
			System.out.println("It is " + activePlayer.getName() + "turn");
			giveToken();// Blockerande funktion, väntar på att få tillbaka token.
			if(activePlayer.hand.getNumberOfCards()==0){
				mb.sendMessage("The winner is: " + activePlayer.getName());
				break;
			}
			setNextPlayer();
		}
		mb.sendMessage("s:HAVEWINNER");
		
	}
	
	/**
	 * Function to give tha token. 
	 * Blocking function
	 * 
	 */
	private synchronized void giveToken(){
		try {
			activePlayer.echoClient("t:");
			activePlayer.hand.setMyTurn(true);
			System.out.print("message sent to" + activePlayer.getName());
			mb.sendMessage("It is " + activePlayer.getName() + "'s turn. Card in play is a "+getTopCardAsString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * funcion to get token back from player
	 * relising the blocking function giveToken
	 * @param player
	 * @return if the turn is valid, if not the player have to keep on playing
	 */
	public synchronized boolean giveTokenBack(Player p){
		if(turnOk){
			if(p.hand.isMyTurn()){
				if(p.hand.getNumberOfCards()<2 && !p.hand.isUno()){
					 giveThreeToActive("m:You have to say \"uno\" before ending your turn if you only have one card left");
				}
				notifyAll();
			}
			p.hand.setMyTurn(false);
			return true;
		}else{
			return false;
		}
		
	}
	
	public synchronized int getNbrOfPlayers(){
		return playerList.size();
	}
	public synchronized int getNbrOfPlayerToStart(){
		return nbrOfPlayerToStart;
	}
	public synchronized void setNbrOfPlayerToStart(int nbrOfPlayerToStart){
		if(nbrOfPlayerToStart>1){
			this.nbrOfPlayerToStart=nbrOfPlayerToStart;
		}else if(playerList.size()>0){
			this.nbrOfPlayerToStart= playerList.size();
		}	
	}
	/**
	 * Function to get status of game
	 * @return string with status
	 */
	public synchronized  String getStatus(Player playerWhoAsked) {
		//RETURNERAR Antalet kort på hand för respektive spelare, aktiv spelare samt topCard åtskiljt av \n
		StringBuilder sb = new StringBuilder();
		int playerWhoAskedIndex = playerList.indexOf(playerWhoAsked);
		sb.append("m:-----------------------------\n");
		for (Player p: playerList) {
			if (playerWhoAsked.equals(p)) {
				sb.append("m:You have "+p.hand.getNumberOfCards()+" cards.\n");	
			}
			else {
			sb.append("m:"+p.getName()+" has "+p.hand.getNumberOfCards()+" cards.\n");	
			}
	
		}
		if (playerWhoAskedIndex==activePlayerIndex) {
			sb.append("m:It is your turn!\n");
		}
		else {
		sb.append("m:Waiting for "+activePlayer.getName()+"...\n");
		}
		//sb.append(deck.peekCardInPlay().getID()+"\n");
		//sb.append("m:Card In play is: ");
		sb.append("m:The top card is a "+getTopCardAsString()  +"\n");
	
		sb.append("m:-----------------------------");
		return sb.toString();
	}
	
	/**
	 * Function to for player to draw a card
	 * @param player to get the card
	 */
	public synchronized void drawACard(Player player){
		try {
			giveCardToPlayer(deck.deal(),player);
			cardsDrawn++;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}if(cardsDrawn>2){
			turnOk=true;
		}
	}
	public synchronized void setWinner(Player winner){
		this.winner=winner;
	}

	
	/**
	 * Function for player to play a card
	 * 
	 * @param playedCardID ID of card up to 108 and new color if needed as 1 to 4 (so plaedCardIndex can be 1083)
	 * @param player	The player that plays the card
	 * @throws IOException
	 */
	synchronized public void playCard(int playedCardID, Player player) throws IOException{
		//playedCardIndex kan vara wildcard + new color
		System.out.print("Server received a card id" + playedCardID);
		int handIndex,newColor;
		
		if(playedCardID<1000){ //Card is not wildCard
			handIndex = player.hand.getHandIndexOf(playedCardID);
			newColor=-1;
		}else{
			newColor=playedCardID%10; //new color is coded in playedCardIndex
			handIndex = player.hand.getHandIndexOf(playedCardID/10); //get the index of the card on hand
		}
		
		if(handIndex==-1){	//handIndex is -1 if card is not on hand
			try {
				player.echoClient("m: you do not have this card, CHEATER!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mb.sendMessage("m: Player " + player.getName() + "tryed to CHEAT! But we give him one more chance");
			
			
		}
		// Where the card is tried to be played
		else{
			Card cardToPlay = player.hand.playCardAtPosition(handIndex); //the card to try to be played.
			if(newColor==-1){
				newColor=cardToPlay.getColor();
			}
			int ans = isCardValid(cardToPlay);
			if(ans==TRUE){		//is card valid
				layNewTopCard(cardToPlay,newColor);	//play the card
				mb.sendMessage("m:"+player.getName() + " played a "+ getTopCardAsString());//message all player what card been played.
				
				System.out.println("cardPlayed. Tcolor= "+ topCardColor + " Tvalue= " + topCardValue + "  Deck CardValues: " + deck.peekCardInPlay().toString());
				turnOk=true;
				
			}else if(ans==WINNER){
				layNewTopCard(cardToPlay,newColor);	//play the card
				mb.sendMessage("m: " + player.getName() + " Won with the card: " + deck.peekCardInPlay());
				setWinner(player);
				mb.sendMessage("s:HAVEWINNER");
				turnOk=true;
			}else if(ans==WRONGPLUS4){
				 giveCardToPlayer(cardToPlay,player);
				//player.echoClient("c:" +new Integer(cardToPlay.getID()).toString());
				giveThreeToActive("m: you can't play the card " + cardToPlay + " if you have other cards you can play");
			}else if(ans==WRONGUNO){
				giveCardToPlayer(cardToPlay,player);
				//player.echoClient("c:" +new Integer(cardToPlay.getID()).toString());
				giveThreeToActive("m: you can't win without saying uno first");
			}else{ // card not valid
				try {
					player.echoClient("m: you can't play that card");
					giveCardToPlayer(cardToPlay,player);
					//player.echoClient("c:" +new Integer(cardToPlay.getID()).toString()); // send back the card
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}		
	}
	private void giveThreeToActive(String message){
		try {
			giveCardToPlayer(deck.deal(),activePlayer);
			giveCardToPlayer(deck.deal(),activePlayer);
			giveCardToPlayer(deck.deal(),activePlayer);
			activePlayer.echoClient(message);	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private synchronized boolean hasWinner(){
		return winner!=null;
	}
	/**
	 * Function to check if card is valid to play
	 * @param card the card to be played
	 * @return true if card is valid to play
	 */
	private int isCardValid(Card card) {
		if(activePlayer.hand.getNumberOfCards()==0 && !activePlayer.hand.isUno()){
			return WRONGUNO;
		}else if (activePlayer.hand.getNumberOfCards()==0){
			return WINNER;
		}
		if(oldValue!=-1){  //OM PLAYER SPELAT ETT KORT INNAN, KOLLA SÅ ATT value stämmer
			if(card.getValue()!=oldValue){
				return FALSE;
			}else{
				return TRUE;
			}
		}else if(card.getValue()==Card.CHANGECOLOR){ //OM DET ÄR PLAYERS FÖRSTA KORT, KOLLA OM DET ÄR BYTA FÄRG KORT
			return TRUE;
		} 
		if (card.getValue() == Card.PLUS4){//ELLER ETT PLUS4-KORT
			for(Card c: activePlayer.hand.getHand()) {
				if (c.getColor()!=Card.WILD) {
					if (isCardValid(c)==TRUE){
						
						return WRONGPLUS4;
					}
				} 
			}
			return TRUE;
		}
		
		if (card.getValue()==topCardValue || card.getColor()==topCardColor) {
			return TRUE;
		}
		return FALSE;	
	}
	
	/**
	 * Function to lay a new card to be active card in play
	 * @param card the card
	 * @param newColor the new color (if card is wild card)
	 * @throws IOException
	 */
	private void layNewTopCard(Card card, int newColor) throws IOException{
			tryAttackPlayer(card);
			topCardValue=card.getValue();
			topCardColor=newColor;
			deck.setCardInPlay(card);
			oldValue=card.getValue();
	
	}
	
	/**
	 * dealStartHands to all players
	 * @throws IOException
	 */
	private void dealStartHands() throws IOException {
		for (int i = 0; i < numberOfPlayers; i++) {
			for (int j = 0; j < 7; j++) {
				Card tempCard = deck.deal();
				giveCardToPlayer(tempCard,playerList.get(i));
			}
			playerList.get(i).echoClient("s:INITDONE"); //send ready to client
		}
		
	}
		
	
	/**
	 * Give a card to a player
	 * @param card to be given
	 * @param player to get the card
	 * @throws IOException
	 */
	private void giveCardToPlayer(Card card, Player player) throws IOException {
		player.hand.addCard(card);
		player.echoClient("c:"+new Integer(card.getID()).toString());
		
		

	}
	
	/**
	 * function to set next player to play
	 */
	private void setNextPlayer() {
		if (clockWise){
			if (activePlayerIndex == numberOfPlayers - 1){
				activePlayerIndex = 0;
			}else{
				activePlayerIndex++;
			}
		}else{
			if (activePlayerIndex == 0){
				activePlayerIndex = numberOfPlayers - 1;
			}else{
				activePlayerIndex--;
			}
		
		}
		activePlayer = playerList.get(activePlayerIndex);
		//return playerList.get(activePlayerIndex);
	}

	
	
	/**
	 * Function to peek at who the next player will be
	 * @return player that will be next
	 */
	private Player peekNextPlayer() {
		if (clockWise){
			if (activePlayerIndex == numberOfPlayers - 1){
				return playerList.get(0);
			}else{
				return playerList.get(activePlayerIndex+1);
			}
		}else{
			if (activePlayerIndex == 0){
				return playerList.get(numberOfPlayers-1);	
				}else{
					return playerList.get(activePlayerIndex-1);
			}
		}
	}
	

	
	/**
	 * Try to attack a player and deal card if vaild
	 * @param attackCard the card in play
	 * @throws IOException
	 */
	private void tryAttackPlayer(Card attackCard) throws IOException {
		if (attackCard.getValue()==Card.STOPP) {
			jumpPlayer=true;
		}else if(attackCard.getValue()==Card.PLUS2) {
			jumpPlayer=true;
			for(int i=0; i<2;i++) {
				giveCardToPlayer(deck.deal(), peekNextPlayer());
			}
		}else if(attackCard.getValue()==Card.PLUS4) {
			jumpPlayer=true;
			for(int i=0; i<4;i++) {
				giveCardToPlayer(deck.deal(), peekNextPlayer());
			}
		}else if(attackCard.getValue()==Card.CHNAGDIRECTION) {
			clockWise=!clockWise;
		}
	}
	
	private String getTopCardAsString() {
		return(activePlayer.hand.getColorAsString(topCardColor)+" "+ activePlayer.hand.getValueAsString(topCardValue));
	}

}
