package Server;

public class MailBox {

	StringBuilder sbChat, sbPlayedCard;
	

	public MailBox() {
		sbChat = new StringBuilder();
		//sbPlayedCard = new StringBuilder();
		
	}
	
	synchronized public void sendMessage(String value) {
		sbChat.append(value);
		sbChat.append("\n");
		notifyAll();
	}
	

	synchronized public String popMessage() throws InterruptedException {
		wait();
		String tempStr = sbChat.toString();
		sbChat.setLength(0);
		return tempStr;
	}

}
