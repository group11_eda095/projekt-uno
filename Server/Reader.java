package Server;

import java.io.IOException;
import java.util.ArrayList;

public class Reader extends Thread {
	ArrayList<Player> clientsList;
	MailBox conn;
	String str;

	public Reader(ArrayList<Player> clientsList, MailBox conn) {
		this.clientsList = clientsList;
		this.conn = conn;
		str = null;
	}

	public void run() {
		while (true) {
			try {

				str = conn.popMessage();

				for (Player c : clientsList) {
					if (c.isAlive()) {
					 try {
							c.echoClient(str);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					else clientsList.remove(c);

				}
				
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

		}

	}
