package Server;
import Components.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ServerMain{

	
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		ArrayList<Player> clientsList = new ArrayList<Player>();
		Socket clientSocket = null;
		ServerSocket serverSocket = null;
		int port;
		if(args.length==0){
			port = 30000;
		}else{
			port = Integer.parseInt(args[0]);
		}
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Server started on port " + port);
		MailBox mb = new MailBox();
		Reader readerThread = new Reader(clientsList, mb);
		readerThread.start();
		
		
		
		//LÄS IN ANTALET SPELARE
		//Scanner s;
		//int players=3;
		/*
		s = new Scanner(System.in);
		while (true) {
			
			System.out.print("Enter number of players (2-4): ");
		try { players = s.nextInt(); }
		catch (InputMismatchException e) {	
		}
		if (players>=2 && players <= 4) {
		
			break;
		}
		System.out.println("Integer between 2 and 4");
		}
		*/
		int gameNbr = 1;
		UnoEngine engine = new UnoEngine(clientsList,mb,gameNbr);
		System.out.println("Waiting for players to connect...");
		
		while(true){
			
			
			try {
				clientSocket = serverSocket.accept();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mb.sendMessage("There is " + (clientsList.size()+1) + " players connected");
			Player thread;
			try {
				thread = new Player(clientSocket, mb,engine,new String("Player "+(clientsList.size()+1)));
				thread.start();
				clientsList.add(thread);
				clientSocket = null;
				//TODO notify number of players, using engine ish.
				
				System.out.println("Player " + clientsList.size() + " has connected.");
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(clientsList.size() >= engine.getNbrOfPlayerToStart()){
				System.out.println("All players connected!");
				engine.start();
				
				System.out.print("Prepareing one more game"); //Trying to prepone more game
				clientsList = new ArrayList<Player>();
				mb = new MailBox();
				readerThread = new Reader(clientsList, mb);
				readerThread.start();
				gameNbr++;
				engine = new UnoEngine(clientsList,mb,gameNbr);
				
				
			}
				
		}
		
				
	}



}
