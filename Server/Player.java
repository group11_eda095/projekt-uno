package Server;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

import Components.Card;
import Components.Hand;

public class Player extends Thread {

	Socket clientSocket = null;
	InputStream is = null;
	OutputStream os = null;
	BufferedReader in = null;
	MailBox mb;
	Hand hand;
	int playedCardID;
	UnoEngine unoEngine;
	boolean ready;
	
	public Player(Socket socket, MailBox mb, UnoEngine engine, String name) throws IOException {
		super(name);
		clientSocket = socket;
		is = clientSocket.getInputStream();
		os = clientSocket.getOutputStream();
		in = new BufferedReader(new InputStreamReader(is));
		this.mb = mb;
		hand = new Hand();
		ready=false;
		unoEngine = engine;
		
	}

	public void run() {

		InetAddress clientId = clientSocket.getInetAddress();
		System.out.println(this.getName() + " is now connected");

		while (!clientSocket.isClosed()) {
			try {
				String str = in.readLine();
				String command = "";
				try {
					command = str.substring(0, 2).toLowerCase();
				} catch (StringIndexOutOfBoundsException e) {

				}
				switch (command) {
				case "n:":
					mb.sendMessage("m:"+this.getName() + " is now known as " + str.substring(2));
					this.setName(str.substring(2));
					setHasName(true);
				break;
				case "c:":
					if(hand.isMyTurn()){ //validating token
						playedCardID = Integer.parseInt(str.substring(2));
						System.out.println("A card with id " + playedCardID);
						
						unoEngine.playCard(playedCardID,this);
					}else{
						System.out.println(this.getName() + "is trying to play a card and not its turn");
						
					}
					break;
				case "m:":
					
					mb.sendMessage("m:"+this.getName()+": "+str.substring(2));
					if (str.substring(2).equals("!uno") || str.substring(2).equals("uno") ||str.substring(2).equals("UNO") ||str.substring(2).equals("!UNO")) {
						echoClient("m:you said uno");
						System.out.println("UNO!!!!");
						hand.setUno(true);
					}else if(str.substring(2).equals("!START")){
						unoEngine.setNbrOfPlayerToStart(0);
						mb.sendMessage("m:Game will start with next player to join");
					}else if(str.substring(2).equals("!PLAYERS?")){
						mb.sendMessage("m:There are " + unoEngine.getNbrOfPlayers() + " players connected");
					}
					
					break;
				case "t:":
					//token återlämnad
					try {
						if(hand.isMyTurn()){
							if(unoEngine.giveTokenBack(this)){
								echoClient("m:your turn is done");
							}else{
								echoClient("m:your have to play a card or draw up to three cards");
								echoClient("t:");
							}
						}else{
							echoClient("m: not your turn");
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case "q:":
					mb.sendMessage("m:"+this.getName() + ": left the server");
					os.close();
					in.close();
					clientSocket.close();
					break;
					
				case "d:":
					if(hand.isMyTurn()){
						unoEngine.drawACard(this);
					}
					break;
				case "s:":
					if(str.equals("s:READY")){ //comand client ready
						setReady(true);
					}
					String tempString = unoEngine.getStatus(this);
					System.out.println("Sends status to "+this+" :"+tempString);
					echoClient(tempString);
					break;
				default:
					break;
				}

			} catch (IOException e) {

				e.printStackTrace();
			}

		}//End of while
		setReady(false); //client disconnected set ready false;

	}
	synchronized public boolean isReady() throws InterruptedException{
		if(ready){
			return true;
		}else{
			wait();
			return false;
		}
	}
	synchronized private void setHasName(boolean ready){
		this.ready = ready;
		notifyAll();
	}
	synchronized public boolean hasName() throws InterruptedException{
		if(ready){
			return true;
		}else{
			wait();
			return false;
		}
	}
	synchronized private void setReady(boolean ready){
		this.ready = ready;
		notifyAll();
	}
	synchronized public void echoClient(String str) throws IOException {
		os.write(str.getBytes());
		os.write('\r');
		os.write('\n');
		os.flush();
	}
	
}
