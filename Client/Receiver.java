package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import Components.Card;
import Components.Deck;
import Components.Hand;

public class Receiver extends Thread{
	BufferedReader in;
	Hand hand;
	ClientInput clientIn; //gets sender to send ack.
	public Receiver(BufferedReader in, Hand h, ClientInput clientIn) {
		this.in=in;
		hand = h;
		this.clientIn=clientIn;
	
	}
	
	synchronized public void run(){
		 String tempString="";
		while (true) { 
				try {
					tempString = in.readLine();
					/*
					if (tempString == null)	System.exit(0);
					else System.out.println(tempString);
					*/
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String command="0";
				try {
					command = tempString.substring(0, 2).toLowerCase();
				} catch (StringIndexOutOfBoundsException e) {
					// nope
				}
				switch (command) {
				case "c:":
					//kort mottaget
					Card c = hand.getRefCard(Integer.parseInt(tempString.substring(2)));
					hand.addCard(c);
					System.out.println("you got a " + c);
					break;
				case "m:":
					System.out.println(tempString.substring(2));				
					break;
				case "s:":
					if(tempString.equals("s:INITDONE")){
						clientIn.setGameStarted(true);
						System.out.println("got all the cards");
						
					}else if(tempString.equals("s:HAVEWINNER")){
						clientIn.setGameStarted(false);
						System.out.println("The game is over, there is a winner");
					}else{
						System.out.print(tempString.substring(2));				
					}
					break;
				case "t:":
					//token mottagen
					System.out.println("---It is your turn!---");
					hand.setMyTurn(true);
					break;

					
				default:
					//System.out.println("Debug. " +tempString);
					break;
				}
			}
		} 
		
		
	

	
}
