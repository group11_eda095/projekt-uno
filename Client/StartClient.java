package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import Components.Hand;
import Server.Player;

public class StartClient {

	
	
	public static void main(String[] args) throws UnknownHostException, IOException {

		Socket clientSocket = null;
		InputStream is = null;
		OutputStream os = null;
		BufferedReader in = null;
		Hand h = new Hand();
		
		if(args.length == 2){
			System.out.println(args[0] + " port; " + args[1]);
			clientSocket = new Socket(args[0], Integer.parseInt(args[1]));
			
		}else{
			System.out.println("start with arguments!: StartClient [adress] [port]");
			System.exit(0);
		}
		
				
		is = clientSocket.getInputStream();
		os = clientSocket.getOutputStream();
		in = new BufferedReader(new InputStreamReader(is));
		
		ClientInput clientIn = new ClientInput(os,h);
		Receiver tReciver = new Receiver(in,h,clientIn);
		
		tReciver.start();
		clientIn.run();
	}

}
