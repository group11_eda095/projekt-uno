package Client;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

import Components.Hand;

public class ClientInput{
	Scanner scan;
	String myLine;
	OutputStream os;
	Hand hand;
	boolean myTurn, gameGoing;

	public ClientInput(OutputStream os, Hand h) {
		this.os = os;
		scan = new Scanner(System.in);
		hand = h;
		gameGoing=false;

	}

	public void run() {
		System.out.println("Write your name:");
		myLine = scan.nextLine();
		send("n:"+myLine);
		printHelpChat();

		String command="";
		while(true) {
			myLine = "default";
			myLine = scan.nextLine();

			if(myLine.length()<2){
				myLine=myLine+":";
			}
			try {
				command = myLine.substring(0, 2).toLowerCase();
			} catch (StringIndexOutOfBoundsException e) {

			}
			if(gameGoing){ //if game Started				 
				switch (command){
				case "h:":
					printHelp();
					break;
				case "m:":
					if(myLine.length()<=2){
						System.out.println("message:");
						myLine="m:" + scan.nextLine();
					}
					try {

						os.write(myLine.getBytes());
						os.write('\r');
						os.write('\n');	
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					break;
				case "l:":
					listCards();
					break;
				case "p:":
					if(hand.isMyTurn()){ //check my turn
						if(hand.getNumberOfCards()>0){ //check that we have cards
							if(myLine.length()<=2){ //check if we wrote p:number, p or p:
								System.out.println("-1: Draw a new card");
								listCards();
								System.out.println("card number?:");
								myLine="p:"+scan.nextLine(); //fix to p:number
							}

							int handIndex =-2;
							try {
								handIndex = Integer.parseInt(myLine.substring(2)); //card in hand
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								System.out.println("You have to play a valid number.");
								break;
							}
							if(handIndex==-1){
								send("d:");
								break;
							}
							if( 0 > handIndex || handIndex >= hand.getNumberOfCards()){ //check if number exist
								System.out.println("Wrong number, nothing played");
								myLine="";
								break;
							}
							int cardIndex = hand.playCardAtPosition(handIndex).getID();
							if(cardIndex>99){
								System.out.print("Change to: \n" +
										"1:RED?\n" +
										"2:GREEN?\n" +
										"3:BLUE?\n" +
										"4:YELLOW?\n");
								cardIndex=((cardIndex*10)+(Integer.parseInt(scan.nextLine())));
							}
							send("c:"+ cardIndex);
							myLine="";
							break;
						}else{
							System.out.println("You have no cards");
							send("d:");
							break;
						}
					}else{
						System.out.print("it is not your turn");
					}

					break;
				case "t:":
					send(myLine);
					hand.setMyTurn(false);
					break;
				case "d:":
					send("d:");
					break;
				case "s:":
					send("s:");
					break;
				case "q:":
					System.out.println("Wait until we have a winner.");
					

					break;
				case "r:":
					printRules();
					break;
				default : 
					break;
				}
			//If game have not started jet	 
			}else{
				switch (command){
				case "h:":
					printHelpChat();
					break;
				case "r:":
					printRules();
					break;
				case "q:":
					if (!gameGoing) {
						System.out.println("Bye");
						send("q:");
						System.exit(0);
						break;
					}
				case "m:":
					if(myLine.length()<=2){
						System.out.println("message:");
						myLine="m:" + scan.nextLine();
					}
				
					try {

						os.write(myLine.getBytes());
						os.write('\r');
						os.write('\n');	
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				default:
					if(myLine.toUpperCase().contains("START")){
						send("m:!START");
					}else if(myLine.toUpperCase().contains("PLAYERS")){
						send("m:!PLAYERS?");
					}else if(myLine.toUpperCase().contains("UNO")){
						send("m:!UNO");
					}
					break;
				}
			}
		}

	}
	public void setGameStarted(boolean gameGoing){
		if(gameGoing){
			System.out.println("Game has started");
			printHelp();

			System.out.println("Do not forget to write \"!uno\" in the chat after");
			System.out.println("you have played your second to last card!!!");
			send("s:READY");
		}
		this.gameGoing = gameGoing;
	}
	public synchronized void send(String s){
		try {
			os.write(s.getBytes());
			os.write('\r');
			os.write('\n');
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printHelp(){
		System.out.println("--------------------------");
		System.out.println("h - Show commands");
		System.out.println("r - UNO rules");
		System.out.println("p = Play card");
		System.out.println("d - Draw card");
		System.out.println("s = Show top card and active player");
		System.out.println("l = List cards on hand");
		System.out.println("t = End turn");
		System.out.println("m = Chat message");
		System.out.println("q = Quit game");
		System.out.println("--------------------------");

	}

	private void printRules(){
System.out.println("--------------------------------------------------------");
System.out.println("The winner of UNO is the player who first plays out all");
System.out.println("of her cards. She must not forget though, to after the");
System.out.println("second to last played cardwrite \"uno\" in the chat.");
System.out.println(" ");

System.out.println("The UNO deck consists of:");
System.out.println("76 numbered cards - value and colour");
System.out.println("8 stop cards - colour");
System.out.println("8 change direction - colour");
System.out.println("8 next player draw 2 cards - colour");
System.out.println("4 change colour - colourless");
System.out.println("4 change colour + next player draw 2 cards - colourless");
System.out.println(" ");
System.out.println("The active player plays a card, matching the colour or the");
System.out.println("value on the top card on the table. If the player has other");
System.out.println("cards on hand with the same value or of the same type");
System.out.println("(e.g. “Change turn”), these can also be played within the");
System.out.println("same turn. If a player has no cards to play, she draws up");
System.out.println("to 3 cardsto find a playable card before her turn ends.");
System.out.println(" ");
System.out.println("The \"change colour\" cards can always be played, while the");
System.out.println("“change colour + next player draw 4 cards” must not be played");
System.out.println("if the player is able to play another card. A player who");
System.out.println("forgets this is punished by drawing 3 cards. If a player player");
System.out.println("plays \"next player draw 2 cards\", the next player draws two");
System.out.println("cards and then the next player goes.");
System.out.println(" ");
System.out.println("If a player forgets to write \"uno\" in the chat after she");
System.out.println("plays her second to last card and ends her turn, she is");
System.out.println("punished by drawing 3 cards.");
System.out.println("--------------------------------------------------------");

	}
	private void printHelpChat(){
		System.out.println("h - Help commands");
		System.out.println("m - Chat message");
		System.out.println("r - UNO rules");
		System.out.println("START - Start the game when the next player connects");
		System.out.println("PLAYERS - print how many player are connected to pregame server");

	}

	private void listCards(){
		if(hand.getNumberOfCards()<1){
			System.out.println("You have no cards");
		}
		for(int i=0;i<hand.getNumberOfCards();i++){
			System.out.println(i+": a " + hand.getCardAtPosition(i));
		}

	}
}
