package Components;

import java.util.Collections;
import java.util.LinkedList;

/**
 * @author viktor
 *
 */
public class Deck {
	//Card[] deck;
	private final int totalCardsNbr = 108;
	private LinkedList<Card> deck;		//List of cards in deck
	private LinkedList<Card> dealt;		//List of cards dealt, to be able to check.
	private LinkedList<Card> discardPile;	//List of cards in descardPile. used as deck after shuffle
	private int cardsUsed;				//Number of cards used since construction
	private Card topCard=null;			//Card that is the card in play

	/**
	 * Construction
	 * Builds the deck and gives the cards unique numbers.
	 * Sets the topCard.
	 */
	public Deck(){
		deck = new LinkedList<Card>();
		dealt = new LinkedList<Card>();
		discardPile = new LinkedList<Card>();

		int index = 0;
		for(int color = 1; color <= 4; color++){
			for(int value = 0; value <=12; value++){
				//deck[index] = new Card(value,color);
				deck.add(new Card(value,color,index));
				index++;
			}
			for(int value = 1; value <=12; value++){
				//deck[index] = new Card(value,color);
				deck.add(new Card(value,color,index));
				index++;
			}
		}
		for(int i=0;i<4; i++){
			deck.add(new Card(Card.PLUS4,Card.WILD,index));
			index++;
			deck.add(new Card(Card.CHANGECOLOR,Card.WILD,index));
			index++;
		}
		cardsUsed=0;
		shuffle();
		topCard=deal();
	}
	
	/**
	 * Schuffles the deck using the decklist and the discardPile
	 * 
	 */
	public void shuffle() {

		/*for ( int i = deck.length-1; i > 0; i-- ) {
            int rand = (int)(Math.random()*(i+1));
            Card temp = deck[i];
            deck[i] = deck[rand];
            deck[rand] = temp;
        }*/
		deck.addAll(discardPile);
		discardPile.clear();
		Collections.shuffle(deck);
	}
	
	/**
	 * Gives the nuber of cards left in the deck
	 * @return
	 */
	public int cardsLeft() {
		//return deck.length - cardsUsed;
		return deck.size();
	}
	
	/**
	 * Deals the topcard in the deck. 
	 * @return Card topcard in the deck
	 */
	public Card deal() {
		if (deck.size()==0){
			//throw new IllegalStateException("No cards are left in the deck.");
			shuffle();
		}
		cardsUsed++;
		Card c = deck.pop();
		dealt.push(c);
		return c;
	}
	
	/**
	 * Return the number of cards used since construction
	 * @return
	 */
	public int cardsUsed(){
		return cardsUsed;
	}
	
	/**
	 * Discards a card and puts it in the discardPile
	 * @param card
	 */
	public void discard(Card card){
		discardPile.push(card);
	}

	/**
	 * Looks at the card in play
	 * @return
	 */
	public Card peekCardInPlay(){
		return topCard;
	}
	
	/**
	 * @param c
	 */
	public void setCardInPlay(Card c){
		discard(topCard);
		topCard=c;
	}
	
	/**
	 * @return
	 */
	public int cardsInHands(){
		return totalCardsNbr-(discardPile.size()+deck.size());
	}
}
