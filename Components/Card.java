package Components;

/**
 * @author viktor
 *
 */
public class Card implements Comparable{

	//Colors
	public final static int WILD = 0;   // Codes for colors + wild
	public final static int RED = 1;
	public final static int GREEN = 2;
	public final static int BLUE = 3;
	public final static int YELLOW = 4;

	//Values
	public final static int CHNAGDIRECTION = 10;  // codes for special cards values
	public final static int STOPP = 11;   
	public final static int PLUS2 = 12;     

	public final static int PLUS4 = 13;
	public final static int CHANGECOLOR = 14;

	
	
	private final int color; //The Color of the card (wild for wild)
	private final int value; //The Value of the card
	private final int id; //The uniq id of the card ( red4 is not same as other red4)

	/**
	 * Constructor
	 * for cards in uno
	 * 
	 * @param theValue value of card
	 * @param theColor color of card
	 * @param theIndex the id of card
	 */
	public Card(int theValue, int theColor, int theIndex) {  
		value = theValue;
		color = theColor;
		id = theIndex;
	}

	/**
	 * Get color
	 * @return int color The color of the card
	 */
	public int getColor() {
		return color;
	}

	/**
	 * @return int value  the value of the card
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @return Color as text 
	 */
	public String getColorAsString() {
		switch ( color ) {
		case WILD:   return "wild";
		case RED:   return "red";
		case GREEN: return "green";
		case BLUE:    return "blue";
		case YELLOW:	return "yellow";
		default:       return "wild";
		}
	}

	/**
	 * @return value as text
	 */
	public String getValueAsString() {
		switch ( value ) {
		case 0:   return "0";
		case 1:   return "1";
		case 2:   return "2";
		case 3:   return "3";
		case 4:   return "4";
		case 5:   return "5";
		case 6:   return "6";
		case 7:   return "7";
		case 8:   return "8";
		case 9:   return "9";
		case 10:  return "ChangeDirection";
		case 11:  return "Stopp";
		case 12:  return "Plus2";
		case 13:  return "Plus4";
		case 14:  return "ChangeColor";
		default:  return "Wrong"; // happy funtion
		}
		
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getColorAsString()+" "+getValueAsString();
	}

	/**
	 * @return id of the card
	 */
	public int getID(){
		return id;
	}
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Object arg0) {
		return id - ((Card)arg0).getID();
	}

	/* (non-Javadoc)
	 * To be able to sort the cards
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * To be able to find a card
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	/**
	 * @return is card a Wild card?
	 */
	public boolean isWildCard() {
		return (id>=100);
	}
	
	
	
}
