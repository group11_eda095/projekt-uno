package Components;

import java.util.ArrayList;

public class Hand {
	
	//Colors
	public final static int WILD = 0;   // Codes for colors + wild
	public final static int RED = 1;
	public final static int GREEN = 2;
	public final static int BLUE = 3;
	public final static int YELLOW = 4;

	//Values
	public final static int CHNAGDIRECTION = 10;  // codes for special cards values
	public final static int STOPP = 11;   
	public final static int PLUS2 = 12;     

	public final static int PLUS4 = 13;
	public final static int CHANGECOLOR = 14;

	
	private ArrayList<Card> cards; // cards in hand
	private Card[] allCards; //All cards for reference
	boolean myTurn;
	boolean uno;

	
	/**
	 * Constructor
	 * 
	 */
	public Hand() {
		cards = new ArrayList<Card>();	
		makeRefCards();
		myTurn=false;
	}
	
	/**
	 * Function to get myTurn token
	 * @param myTurn boolean token
	 */
	public synchronized void setMyTurn(boolean myTurn){
		this.myTurn=myTurn;
	}
	
	/**
	 * Function to check token
	 * @return
	 */
	public synchronized boolean isMyTurn(){
		return myTurn;
	}

	
	/**
	 * Function to add card
	 * @param c
	 */
	public synchronized void addCard(Card c) {
		uno=false;
		cards.add(c);
	}
	 
	
	/**
	 * @param c
	 * @return
	 */
	public synchronized boolean removeCard(Card c) {
		return cards.remove(c);
	}
	
	/**
	 * @return
	 */
	public ArrayList<Card> getHand() {
		return  cards;
	}
	
	/**
	 * @return
	 */
	public synchronized boolean isUno() {
		return uno;
	}
	public synchronized void setUno(boolean uno){
		this.uno=uno;
	}
	/**
	 * Get the position of a card with specific id
	 * @param cardIndex
	 * @return
	 */
	public int getHandIndexOf(int cardID){
		return cards.indexOf(getRefCard(cardID));
	}
	
	/**
	 * Shows number of cards in hand
	 * @return number of cards in hand
	 */
	public int getNumberOfCards() {
		return cards.size();
	}
	
	/**
	 * Return a card from hand at a position in hand.
	 * @param position
	 * @return Card at position
	 */
	public synchronized Card getCardAtPosition(int position) {
		return cards.get(position);
	}
	/**
	 * Return a card in the hand at position
	 * @param position position index of card in hand.
	 * @return
	 */
	public synchronized Card playCardAtPosition(int position){
		return cards.remove(position);
	}
	
	/**
	 * Return Reference Card with id 
	 * @param id
	 * @return
	 */
	public Card getRefCard(int id){
		return allCards[id];
	}
	/**
	 * Return Color of card as text
	 * @param Color
	 * @return
	 */
	public String getColorAsString(int Color) {
		switch ( Color ) {
		case WILD:   return "wild";
		case RED:   return "red";
		case GREEN: return "green";
		case BLUE:    return "blue";
		case YELLOW:	return "yellow";
		default:       return "wild";
		}
	}
	/**
	 * Return value of card as text
	 * @param value
	 * @return
	 */
	public String getValueAsString(int value) {		
		switch ( value ) {
		case 0:   return "0";
		case 1:   return "1";
		case 2:   return "2";
		case 3:   return "3";
		case 4:   return "4";
		case 5:   return "5";
		case 6:   return "6";
		case 7:   return "7";
		case 8:   return "8";
		case 9:   return "9";
		case 10:  return "ChangeDirection";
		case 11:  return "Stopp";
		case 12:  return "Plus2";
		case 13:  return "Plus4";
		case 14:  return "ChangeColor";
		default:  return "Wrong"; // happy funtion
		}
	}

	
	/**
	 * Function to make the refcard deck
	 */
	public void makeRefCards(){
		allCards = new Card[108];	
		int index = 0;
		for(int color = 1; color <= 4; color++){
			for(int value = 0; value <=12; value++){
				allCards[index] = new Card(value,color,index);
				index++;
			}
			for(int value = 1; value <=12; value++){
				allCards[index] = (new Card(value,color,index));
				index++;
			}
		}
		for(int i=0;i<4; i++){
			allCards[index] = new Card(Card.PLUS4,Card.WILD,index);
			index++;
			allCards[index] = new Card(Card.CHANGECOLOR,Card.WILD,index);
			index++;
			
		}
	}
	

}